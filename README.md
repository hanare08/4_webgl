# README

* **Learning WebGL** aiming at visualization of Shallow Water Simulation.
* See <http://hanare08.bitbucket.org/index.html> for demos.
* Local opening of these html files will fail to use texture images, so use py_server.py or host other local server to display WebGL demos normally.