/**
 * 1-Dimension Simple Wave Visualization using balls,
 * owing to three.js and dat.gui.js
 */

/* global THREE, document, window, requestAnimationFrame */

let width;
let height;
var renderer;
var scene;
var camera;
var ambient;
var cameraControls;

const vertexNumRow = 100;
const vertexNumColumn = 100;
const vertexNum = vertexNumRow * vertexNumColumn;

const frameNum = 100;
var heightMatrix;
const initialHeights = new Array(vertexNum).fill(0.0);

let count = 0;

var hScale = 20;

var textureCube;

var waveBufferGeometry;
var waveMaterial;
var waveMesh;

init();
anime();

/**
 * Initialization.
 */
function init() {
  // domain settings
  width = window.innerWidth;
  height = window.innerHeight;

  // renderer generation
  renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
  renderer.setSize(width, height);
  renderer.setClearColor('#000000', 1);
  renderer.setPixelRatio(window.devidePixelRatio);
  renderer.setFaceCulling(THREE.CullFaceNone);
  document.body.appendChild(renderer.domElement);

  // scene setting
  scene = new THREE.Scene();

  // camera settings
  var fov = 75;
  var aspect = width / height;
  var near = 0.01;
  var far = 100000;
  camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
  camera.position.set(0, 100, 100);
  camera.lookAt(0, 0, 0);
  scene.add(camera);

  // use example camera control
  cameraControls = new THREE.OrbitControls(camera);

  // light settings
  ambient = new THREE.AmbientLight('#eeeeee');
  scene.add(ambient);

  // Object 3D
  waveMaterial = new THREE.ShaderMaterial(
    {
      vertexShader: document.getElementById('vertexShader').textContent,
      fragmentShader: document.getElementById('fragmentShader').textContent,
      uniforms: {
        color: { type: 'c', value: new THREE.Color(0, 255, 255) },
        hScale: { type: 'f', value: hScale }
      },
      side: THREE.DoubleSide
    });
  // waveMaterial = new THREE.MeshBasicMaterial({ side: THREE.DoubleSide });

  waveBufferGeometry = new THREE.PlaneBufferGeometry(2 * vertexNumRow, 2 * vertexNumColumn, vertexNumRow - 1, vertexNumColumn - 1);

  waveBufferGeometry = new THREE.PlaneBufferGeometry(2 * vertexNumRow, 2 * vertexNumColumn, vertexNumRow - 1, vertexNumColumn - 1);
  // let heightAttribute = new THREE.BufferAttribute(initialHeights, 1, false);
  // waveBufferGeometry.addAttribute('nextHeight', heightAttribute);

  waveMesh = new THREE.Mesh(waveBufferGeometry, waveMaterial);
  waveMesh.rotation.x = -Math.PI / 2;
  scene.add(waveMesh);

  heightMatrix = new Array(frameNum);
  for (let frameIdx = 0; frameIdx < frameNum; frameIdx++) {
    heightMatrix[frameIdx] = [];
    for (let i = 0; i < vertexNum; i++) {
      heightMatrix[frameIdx].push(Math.sin(2.0 * Math.PI * (i / vertexNum + frameIdx * 0.01)));
    }
  }
}

/**
 * Update camera aspect when window is reszied.
 */
function onWindowResize() {
  width = window.innerWidth;
  height = window.innerHeight;
  camera.aspect = width / height;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
}

/**
 * Rendering function, in which information of positions, ray caster, camera control are updated.
 */
function render() {
  // position update
  var countIdx = count % frameNum;
  for (let i = 0; i < vertexNum; i++) {
    waveBufferGeometry.attributes.position.setZ(i, hScale * heightMatrix[countIdx][i]);
  }
  waveBufferGeometry.attributes.position.needsUpdate = true;
  waveMaterial.needsUpade = true;

  cameraControls.update();

  renderer.render(scene, camera);
}

/**
 * Animate using render() function every frame, and make loop by call itself .
 */
function anime() {
  count++;
  requestAnimationFrame(anime);
  render();
}
