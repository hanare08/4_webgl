/**
 * cube rotation
 */

/* global THREE, document, window, requestAnimationFrame */

let cube;
let renderer;
let scene;
let camera;

initialize();
animate();

/**
 * Initialization.
 */
function initialize() {
  // domain settings read from window object
  const width = window.innerWidth;
  const height = window.innerHeight;

  // renderer setup
  renderer = new THREE.WebGLRenderer();
  renderer.setSize(width, height);
  renderer.setClearColor(0x000000, 1);
  document.body.appendChild(renderer.domElement);

  // scene setting
  scene = new THREE.Scene();

  // camera settings
  const aspect = width / height;
  const fov = 75;
  const near = 1;
  const far = 10000;
  camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
  camera.position.set(0, 0, 100);
  camera.lookAt(0, 0, 0);
  scene.add(camera);

  // light settings
  const ambient = new THREE.AmbientLight(0x333333);
  scene.add(ambient);


  // object3D setting
  const geometry = new THREE.BoxGeometry(30, 30, 30);
  const material = new THREE.MeshNormalMaterial();
  cube = new THREE.Mesh(geometry, material);
  scene.add(cube);
}

/**
 * Rendering function, in which information of cube is updated.
 */
function render() {
  cube.rotation.y += 0.01;
  cube.rotation.z += 0.005;
  renderer.render(scene, camera);
}

/**
 * Animate using render() function every frame, and make loop by call itself .
 */
function animate() {
  requestAnimationFrame(animate);
  render();
}
