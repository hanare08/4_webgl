/**
 * 1-Dimension Simple Wave Visualization using PlaneBufferGeometry and custom shaders,
 * owing to three.js and dat.gui.js
 */

/* global THREE, document, window, dat, requestAnimationFrame, XMLHttpRequest, Stats*/

var stats;

let width;
let height;
var renderer;
var scene;
var camera;
var sunLight;
var flare;
var ambient;
var cameraControls;
var raycaster;
var mouse;
var controller;
var act = true;
var art = false;

const vertexNumRow = 100;
const vertexNumColumn = 100;
const vertexNum = vertexNumRow * vertexNumColumn;

const frameNum = 100;
var heightMatrix;

let countLoaded;
let loaded;

let count = 0;
let countSub = 0;
let visualizeInterval = 2;

const datPath = 'dat/linear2d/';
const datName = 'wave2D_A_';
const datExtension = '.dat';

const loader = new THREE.TextureLoader();

var hScale;

var textureCube;
var INTERSECTED = null;
var waveBufferGeometry;
var waveMaterial;

init();
anime();

/**
 * Initialization.
 */
function init() {
  // perfomance monitor setting
  stats = new Stats();
  document.body.appendChild(stats.dom)

  // domain settings
  width = window.innerWidth;
  height = window.innerHeight;

  // renderer generation
  renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
  renderer.setSize(width, height);
  renderer.setClearColor('#000000', 1);
  renderer.setPixelRatio(window.devidePixelRatio);
  renderer.setFaceCulling(THREE.CullFaceNone);
  document.body.appendChild(renderer.domElement);

  // texture cube for scene background and envMap.
  var boxDir = 'images/skybox/';
  var urls = [boxDir + 'px.jpg',
    boxDir + 'nx.jpg',
    boxDir + 'py.jpg',
    boxDir + 'ny.jpg',
    boxDir + 'pz.jpg',
    boxDir + 'nz.jpg'];
  textureCube = new THREE.CubeTextureLoader().load(urls);
  textureCube.format = THREE.RGBFormat;
  textureCube.mapping = THREE.RefractionMapping;

  // scene setting
  scene = new THREE.Scene();
  // scene.background = textureCube;

  // dat.GUI controller setup
  controller = {
    'baseColor': '#0012c0',
    'shininess': 15,
    'reflectivity': 1.00,
    'refractionRatio': 0.00,
    'ambientColor': '#aaaaaa',
    'framePerRender': 1,
    'wireFrame': false,
    'start / stop': () => {
      act = !act;
    },
    'artiful background': () => {
      if (art) {
        scene.background = null;
        scene.remove(flare);
      } else {
        scene.background = textureCube;
        scene.add(flare);
      }
      art = !art;
    },
    'hScale': 20
  };
  var gui = new dat.GUI();
  gui.remember(controller);
  let folderPhysics = gui.addFolder('Physics Control');
  folderPhysics.add(controller, 'framePerRender', 1, 10).step(1).onChange(
    function () {
      visualizeInterval = Number(controller.framePerRender);
    }
  );
  folderPhysics.add(controller, 'start / stop');
  var folderVisual = gui.addFolder('Visual Control');
  folderVisual.add(controller, 'artiful background');
  folderVisual.add(controller, 'hScale', 0.0, 100, 1).onChange(
    function () {
      hScale = controller.hScale;
      // waveMaterial.uniforms.hScale.value = hScale;
      render();
    }
  );

  folderVisual.addColor(controller, 'ambientColor').onChange(
    function () {
      ambient.color.set(controller.ambientColor);
      render();
    }
  );
  folderVisual.addColor(controller, 'baseColor').onChange(
    function () {
      waveMaterial.color.set(controller.baseColor);
      waveMaterial.needsUpdate = true;
      render();
    }
  );
  folderVisual.add(controller, 'wireFrame').onChange(
    function () {
      waveMaterial.wireframe = controller.wireFrame;
      waveMaterial.needsUpdate = true;
      render();
    }
  );

  folderVisual.open();
  folderPhysics.open();

  // camera settings
  var fov = 75;
  var aspect = width / height;
  var near = 0.01;
  var far = 100000;
  camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
  camera.position.set(100, 100, -100);
  camera.lookAt(0, 0, 0);
  scene.add(camera);

  // use example camera control
  cameraControls = new THREE.OrbitControls(camera);

  // light settings
  ambient = new THREE.AmbientLight(controller.ambientColor);
  scene.add(ambient);

  // sun and lensFlare settings
  sunLight = new THREE.PointLight(0xffffff, 2.0);
  sunLight.position.set(-1000, 700, 1000);
  scene.add(sunLight);
  var textureFlare0 = loader.load('images/lensflare0.png');
  var textureFlare2 = loader.load('images/lensflare2.png');
  var textureFlare3 = loader.load('images/lensflare3.png');
  var flareColor = new THREE.Color(0xffffff);
  flare = new THREE.LensFlare(textureFlare0, 700, 0.0, THREE.AdditiveBlending, flareColor);
  flare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
  flare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
  flare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
  flare.add(textureFlare3, 60, 0.6, THREE.AdditiveBlending);
  flare.add(textureFlare3, 70, 0.7, THREE.AdditiveBlending);
  flare.add(textureFlare3, 120, 0.9, THREE.AdditiveBlending);
  flare.add(textureFlare3, 70, 1.0, THREE.AdditiveBlending);
  flare.position.copy(sunLight.position);
  flare.customUpdateCallback = lensFlareUpdateCallback;
  // scene.add(flare);

  // raycaseter and mouse settings
  raycaster = new THREE.Raycaster();
  mouse = new THREE.Vector2();
  window.addEventListener('mousemove', onMouseMove, false);

  // other default values
  hScale = controller.hScale;

  // Object 3D
  // waveMaterial = new THREE.ShaderMaterial(
  //   {
  //     vertexShader: document.getElementById('vertexShader').textContent,
  //     fragmentShader: document.getElementById('fragmentShader').textContent,
  //     uniforms: {
  //       color: { type: 'c', value: new THREE.Color(controller.baseColor) },
  //       hScale: { type: 'f', value: hScale }
  //     }
  //   });

  waveMaterial = new THREE.MeshPhongMaterial(
    {
      color: controller.baseColor,
      transparent: true,
      opacity: 0.9,
      blending: THREE.AdditiveBlending,
      side: THREE.DoubleSide,
      depthTest: false,
      envMap: textureCube,
      reflectivity: 1.0,
      refractionRatio: 0.8
      // vertexShader: document.getElementById('vertexShader').textContent,
      // fragmentShader: document.getElementById('fragmentShader').textContent,
      // uniforms: {
      //   color: { type: 'c', value: new THREE.Color(controller.baseColor) },
      //   hScale: { type: 'f', value: hScale }
      // }
    });



  waveBufferGeometry = new THREE.PlaneBufferGeometry(2 * vertexNumRow, 2 * vertexNumColumn, vertexNumRow - 1, vertexNumColumn - 1);
  // let heightAttribute = new THREE.BufferAttribute(initialHeights, 1, false);
  // waveBufferGeometry.addAttribute('nextHeight', heightAttribute);

  let waveMesh = new THREE.Mesh(waveBufferGeometry, waveMaterial);
  waveMesh.rotation.x = -Math.PI / 2;
  scene.add(waveMesh);

  // request .dat files
  heightMatrix = new Array(frameNum);
  countLoaded = 0;
  loaded = false;

  for (let i = 0; i < frameNum; i++) {
    let datID = ('000' + i).slice(-4);
    let request = new XMLHttpRequest();
    request.myID = i;
    request.onload = dataHandler;
    request.open('GET', datPath + datName + datID + datExtension, true);
    request.send();
  }

  window.addEventListener('resize', onWindowResize, false);
}

/**
 * Receives a HTTP Response, and convert it(string!) into managable data format.
 * this.myID means its index of heightMatrix.
 */
function dataHandler() {
  if (this.status === 200) {
    if (++countLoaded >= frameNum) {
      loaded = true;
    }
    // console.log('HTTP Request with ID=' + this.myID + ' succeeded with status:' + this.status);
    // console.log(this.getAllResponseHeaders());
    heightMatrix[this.myID] = parseResponse(this.responseText);
    // console.log('type of dat is ' + typeof (heightMatrix[this.myID]));
  } else {
    console.log('HTTP Request with ID=' + this.myID + ' failed with status' + this.status);
  }
}

/**
 * Parser function for Simple HTTP server's response.
 * @param {string} raw: Raw respponse text string.
 * @return {stringArray} heights: size=vertexNum
 */
function parseResponse(raw) {
  let triples = raw.split('\n').slice(1, vertexNum + 1);
  let heights = triples.map(triple => parseFloat((triple.split('\t'))[2]));
  return heights;
}

/**
 * Gets mouse position (x,y) when mouse move event occurs, called by addEventListener.
 * @param {MouseEvent} event MouseEvent object generated when mouse moves.
 */
function onMouseMove(event) {
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
}

/**
 * Update camera aspect when window is reszied.
 */
function onWindowResize() {
  width = window.innerWidth;
  height = window.innerHeight;
  camera.aspect = width / height;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
}

/**
 * Used for custom update of lensFlare.
 * @param{object3D} object : lensFlare object.
 */
function lensFlareUpdateCallback(object) {
  var fl = object.lensFlares.length;
  var lensFlare;
  var vecX = -object.positionScreen.x * 2;
  var vecY = -object.positionScreen.y * 2;
  for (var f = 0; f < fl; f++) {
    lensFlare = object.lensFlares[f];
    lensFlare.x = object.positionScreen.x + vecX * lensFlare.distance;
    lensFlare.y = object.positionScreen.y + vecY * lensFlare.distance;
    lensFlare.rotation = 0;
  }
  object.lensFlares[2].y += 0.025;
  object.lensFlares[3].rotation = object.positionScreen.x * 0.5 + THREE.Math.degToRad(45);
}

/**
 * Rendering function, in which information of positions, ray caster, camera control are updated.
 */
function render() {
  // position update
  var countIdx = count % frameNum;
  if (loaded) {
    for (let i = 0; i < vertexNum; i++) {
      waveBufferGeometry.attributes.position.setZ(i, hScale * heightMatrix[countIdx][i]);
    }
    waveBufferGeometry.attributes.position.needsUpdate = true;
  }

  cameraControls.update();
  stats.update();

  raycaster.setFromCamera(mouse, camera);
  var intersects = raycaster.intersectObjects(scene.children);
  if (intersects.length > 0) {
    if (INTERSECTED) {
      // INTERSECTED.material = materialBall;
      if (INTERSECTED.object !== intersects[0].object) {

        INTERSECTED = intersects[0];
        // INTERSECTED.material = materialBallEmissive;
      }
    } else {
      INTERSECTED = intersects[0];
    }

  } else if (INTERSECTED) {
    // INTERSECTED.material = materialBall;
    INTERSECTED = null;
  }
  renderer.render(scene, camera);
}

/**
 * Animate using render() function every frame, and make loop by call itself .
 */
function anime() {
  if (act && loaded) {
    countSub = (countSub + 1) % visualizeInterval;
    if (countSub === 0) {
      count++;
    }
  }
  requestAnimationFrame(anime);
  render();
}
