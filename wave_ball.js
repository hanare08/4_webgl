/**
 * 1-Dimension Simple Wave Visualization using balls,
 * owing to three.js and dat.gui.js
 */

/* global THREE, document, window, dat, requestAnimationFrame, XMLHttpRequest, Stats*/
let stats;

let renderer;
let scene;
let camera;
let cameraControls;
let raycaster;
let mouse;
let controller;
let yController;
let act = true;
let frameControlling = false;

const ballNumRow = 100;
const ballNumColumn = 100;
const ballNum = ballNumRow * ballNumColumn;

let ballMatrix = [];

const frameNum = 100;
let datMatrix;

let count = 0;

const datPath = 'dat/linear2d/';
const datName = 'wave2D_A_';
const datExtension = '.dat';

let INTERSECTED = null;
let savedMaterial = null;
let OBSERVING = null;

let materialBall;
let materialBallEmissive1;
let materialBallEmissive2;

initialize();
animate();

/**
 * Initialization.
 */
function initialize() {
  // performance monitor setting
  stats = new Stats();
  document.body.appendChild(stats.dom);

  // renderer generation
  renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.setClearColor('#000000', 1);
  renderer.setPixelRatio(window.devidePixelRatio);
  renderer.setFaceCulling(THREE.CullFaceNone);
  document.body.appendChild(renderer.domElement);

  // scene setting
  scene = new THREE.Scene();

  // camera settings
  const fov = 75;
  const aspect = window.innerWidth / window.innerHeight;
  const near = 1;
  const far = 2000;
  camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
  camera.position.set(100, 100, -100);
  camera.lookAt(0, 0, 0);
  scene.add(camera);

  // use example camera control
  cameraControls = new THREE.OrbitControls(camera);

  // light settings
  let light = new THREE.DirectionalLight(0xffffff, 2.0);
  light.position.set(-2, 1, 2);
  scene.add(light);
  let ambient = new THREE.AmbientLight('#aaaaaa');
  scene.add(ambient);

  // raycaseter and mouse settings
  raycaster = new THREE.Raycaster();
  mouse = new THREE.Vector2();
  window.addEventListener('mousemove', onMouseMove, false);
  window.addEventListener('dblclick', onMouseDoubleClick, false);
  window.addEventListener('resize', onWindowResize, false);

  // GUI controller setup
  controller = {
    'ballColor': '#eeeeee',
    'start / stop': () => {
      act = !act;
    },
    'yscale': 20,
    'frameID': 0,
    'ballX': 0.01,
    'ballY': 0.01,
    'ballZ': 0.01
  };
  let gui = new dat.GUI();
  gui.add(controller, 'start / stop');
  let frameController = gui.add(controller, 'frameID').min(0).max(frameNum).step(1).listen();
  frameController.onChange(function() {
    frameControlling = true;
    count = controller.frameID;
    render();
  });

  frameController.onFinishChange(function() {
    frameControlling = false;
  });

  let folderPhysics = gui.addFolder('Physics Control');
  folderPhysics.open();

  let folderVisual = gui.addFolder('Visual Control');
  folderVisual.open();
  folderVisual.addColor(controller, 'ballColor').onChange(() =>
    materialBall.color.set(controller.ballColor)
  );
  let folderInfo = gui.addFolder('Ball Information');
  folderInfo.open();

  folderInfo.add(controller, 'ballX').min(-ballNumRow).max(ballNumRow).listen();
  yController = folderInfo.add(controller, 'ballY').step(0.01).max(controller.yscale * 3.0).min(controller.yscale * (-1.5)).listen();
  folderInfo.add(controller, 'ballZ').min(-ballNumColumn).max(ballNumColumn).listen();

  folderPhysics.add(controller, 'yscale', 0.0, 100, 1).onChange(() => {
    yController.max(controller.yscale * 3.0);
    yController.min(controller.yscale * (-1.5));
  });

  // geometry and material preparation
  let geometryBall = new THREE.SphereGeometry(1, 10, 10);
  materialBall = new THREE.MeshStandardMaterial({
    color: controller.ballColor
  });
  materialBallEmissive1 = materialBall.clone();
  materialBallEmissive1.emissive.set('#880000');
  materialBallEmissive2 = materialBall.clone();
  materialBallEmissive2.emissive.set('#00cc44');

  // mesh position initialization
  for (let i = 0; i < ballNumRow; i++) {
    let ballArray = [];
    for (let j = 0; j < ballNumColumn; j++) {
      let ball = new THREE.Mesh(geometryBall, materialBall);
      ball.position.x = i * 2 - ballNumRow;
      ball.position.y = 0;
      ball.position.z = j * 2 - ballNumColumn;
      ballArray.push(ball);
      scene.add(ball);
    }
    ballMatrix.push(ballArray);
  }

  // request .dat files
  datMatrix = new Array(frameNum);
  for (let i = 0; i < frameNum; i++) {
    let request = new XMLHttpRequest();
    request.myID = i;
    request.onload = dataHandler;
    let datID = ('000' + i).slice(-4);
    request.open('GET', datPath + datName + datID + datExtension, true);
    request.send();
  }
}

/**
 * Receives a HTTP Response, and convert it(string!) into managable data format.
 * this.myID means its index of datMatrix.
 */
function dataHandler() {
  if (this.readyState === 4) {
    if (this.status === 200) {
      console.log('HTTP Request with ID=' + this.myID + ' succeeded with status:' + this.status);
      console.log(this.getAllResponseHeaders());
      datMatrix[this.myID] = parseResponse(this.responseText);
      // console.log('type of dat is ' + typeof (datMatrix[this.myID]));
    } else {
      console.error('HTTP Request with ID=' + this.myID + ' failed with status:' + this.status);
    }
  }
}

/**
 * Parser function for Simple HTTP server's response.
 * @param {string} raw: Raw respponse text string.
 * @return {NumberArray}: size=ballNum
 */
function parseResponse(raw) {
  let pairs = raw.split('\n').slice(1, ballNum + 1);
  let heights = pairs.map(pair => Number((pair.split('\t'))[2]));
  let dataMat = [];
  for (let i = 0; i < ballNumRow; i++) {
    dataMat.push(heights.slice(i * ballNumRow, (i + 1) * ballNumRow));
  }
  return dataMat;
}

/**
 * Gets mouse position (x,y) when mouse move event occurs, called by addEventListener.
 * @param {MouseEvent} event MouseEvent object generated when mouse moves.
 */
function onMouseMove(event) {
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
}

/**
 * Update camera aspect when window is reszied.
 */
function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
}

/**
 * Register current INTERSECTED object as a new OBSERVING object.
 */
function onMouseDoubleClick() {
  if (OBSERVING) {
    // Old ball exists, and reset its information.
    OBSERVING.material = materialBall;
  }
  if (INTERSECTED) {
    // New ball exists, register its infromation.
    INTERSECTED.material = materialBallEmissive2;
    savedMaterial = materialBallEmissive2;
    OBSERVING = INTERSECTED;
  } else {
    OBSERVING = null;
  }
}

/**
 * Rendering function, in which information of positions, ray caster, camera control are updated.
 */
function render() {
  // position update
  if (act && !frameControlling) {
    count = ++count % frameNum;
    controller.frameID = count;
  }
  if (datMatrix[count]) {
    for (let i = 0; i < ballNumRow; i++) {
      for (let j = 0; j < ballNumColumn; j++) {
        ballMatrix[i][j].position.setY(controller.yscale * datMatrix[count][i][j]);
      }
    }
  }

  cameraControls.update();
  stats.update();

  raycaster.setFromCamera(mouse, camera);
  const intersects = raycaster.intersectObjects(scene.children);
  if (intersects.length > 0) {
    if (INTERSECTED !== intersects[0].object) {
      if (INTERSECTED) {
        INTERSECTED.material = savedMaterial;
      }
      INTERSECTED = intersects[0].object;
      savedMaterial = INTERSECTED.material;
      INTERSECTED.material = materialBallEmissive1;
    }
  } else if (INTERSECTED) {
    INTERSECTED.material = savedMaterial;
    INTERSECTED = null;
  }

  if (OBSERVING) {
    controller.ballX = OBSERVING.position.x;
    controller.ballY = OBSERVING.position.y;
    controller.ballZ = OBSERVING.position.z;
  }

  renderer.render(scene, camera);
}

/**
 * Animate using render() function every frame, and make loop by call itself .
 */
function animate() {
  requestAnimationFrame(animate);
  render();
}
