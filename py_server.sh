#! /bin/bash.exe
# used for Web Service test on local server.

python_path="/c/Python27"
python_name="python.exe"
python_opt="-m SimpleHTTPServer"
port_num="8800"

if  [ $# -eq 1 ]; then
	port_num=$1
elif [ $# -ne 0 ]; then
	echo "Wrong number of arguments. It must be 0 or 1.\n";
	echo "Usage: py_server.sh (PORT NUMBER)?";
	exit 1;
fi

${python_path}/${python_name} ${python_opt} ${port_num}