/**
 * Earth visualization using three.js and dat.gui.js
 */

/* global THREE, document, window, dat, requestAnimationFrame */

var earth;
var cloud;

var width;
var height;
var renderer;
var scene;
var camera;
var pointLight1;
var pointLight2;
var sunLight;
var flare;
var ambient;
var controls;
var raycaster;
var mouse;
var group;
var propertyController;

var loader = new THREE.TextureLoader();
// var mapColorLoaded = loader.load('images/land_ocean_ice_cloud_2048.jpg');
var mapColorLoaded = loader.load('images/earth_atmos_2048.jpg');
var mapSpecLoaded = loader.load('images/earth_specular_2048.jpg');
var mapNormalLoaded = loader.load('images/earth_normal_2048.jpg');

var INTERSECTED = null;

init();
anime();

/**
 * Initialization.
 */
function init() {
  // domain settings
  width = window.innerWidth;
  height = window.innerHeight;

  // renderer generation
  renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
  renderer.setSize(width, height);
  renderer.setClearColor(0x000000, 1);
  document.body.appendChild(renderer.domElement);

  // scene setting
  scene = new THREE.Scene();

  // GUI controller setup
  propertyController = {
    rotateSpeed: 0.001,
    defaultColor: '#888888',
    selectedColor: '#cccccc',
    emissiveColor: '#000000',
    shininess: 15,
    colorMap: true,
    specularMap: true,
    normalMap: true,
    reflectivity: 1.00,
    refractionRatio: 0.00,
    opacity: 1.0,
    lx: 0.0,
    ly: 0.0,
    lz: -1.000,
    ambientColor: '#111111'
  };
  var guiController = new dat.GUI();
  guiController.remember(propertyController);
  var folderMaterial = guiController.addFolder('Material Control');
  folderMaterial.addColor(propertyController, 'defaultColor').onChange(render);
  folderMaterial.addColor(propertyController, 'selectedColor').onChange(render);
  folderMaterial.addColor(propertyController, 'emissiveColor').onChange(controlerUpdate);
  folderMaterial.add(propertyController, 'shininess', 0, 100).step(1).onChange(controlerUpdate);
  folderMaterial.add(propertyController, 'colorMap').onChange(createNewEarth);
  folderMaterial.add(propertyController, 'specularMap').onChange(createNewEarth);
  folderMaterial.add(propertyController, 'normalMap').onChange(createNewEarth);
  folderMaterial.add(propertyController, 'reflectivity', 0.0, 1.0).step(0.01).onChange(controlerUpdate);
  folderMaterial.add(propertyController, 'refractionRatio', 0.0, 1.0).step(0.01).onChange(controlerUpdate);
  folderMaterial.add(propertyController, 'opacity', 0.0, 1.0).step(0.01).onChange(controlerUpdate);
  var folderPhysics = guiController.addFolder('Physics Control');
  folderPhysics.add(propertyController, 'rotateSpeed', -0.1, 0.1).step(0.0005).onChange(render);
  folderMaterial.open();
  folderPhysics.open();
  var folderLights = guiController.addFolder('Light Control');
  folderLights.add(propertyController, 'lx', -1.0, 1.0, 0.025).name('Sun Direction x').onChange(controlerUpdate);
  folderLights.add(propertyController, 'ly', -1.0, 1.0, 0.025).name('Sun Direction y').onChange(controlerUpdate);
  folderLights.add(propertyController, 'lz', -1.0, 1.0, 0.025).name('Sun Direction z').onChange(controlerUpdate);
  folderLights.addColor(propertyController, 'ambientColor').onChange(controlerUpdate);

  // camera settings
  var fov = 75;
  var aspect = width / height;
  var near = 1;
  var far = 10000;
  camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
  camera.position.set(0, 0, 100);
  scene.add(camera);

  // use example camera control
  controls = new THREE.OrbitControls(camera);

  // light settings
  ambient = new THREE.AmbientLight(0x111111);
  pointLight1 = new THREE.PointLight(0xb4e7f2, 1.2);
  pointLight2 = new THREE.PointLight(0xb4e7f2, 1.4);
  scene.add(ambient);

  // sun and lensFlare settings
  sunLight = new THREE.PointLight(0xffffff, 2.0);
  sunLight.position.set(5, 5, -1000);
  scene.add(sunLight);
  var textureFlare0 = loader.load('images/lensflare0.png');
  var textureFlare2 = loader.load('images/lensflare2.png');
  var textureFlare3 = loader.load('images/lensflare3.png');
  var flareColor = new THREE.Color(0xffffff);
  flare = new THREE.LensFlare(textureFlare0, 700, 0.0, THREE.AdditiveBlending, flareColor);
  flare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
  flare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
  flare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
  flare.add(textureFlare3, 60, 0.6, THREE.AdditiveBlending);
  flare.add(textureFlare3, 70, 0.7, THREE.AdditiveBlending);
  flare.add(textureFlare3, 120, 0.9, THREE.AdditiveBlending);
  flare.add(textureFlare3, 70, 1.0, THREE.AdditiveBlending);
  flare.position.copy(sunLight.position);
  scene.add(flare);

  // raycaseter and mouse settings
  raycaster = new THREE.Raycaster();
  mouse = new THREE.Vector2();
  window.addEventListener('mousemove', onMouseMove, false);

  createNewEarth();

  // Cloud Sphere
  var boxName = 'images/skybox_bluecloud/';
  var urls = [boxName + 'px.jpg',
    boxName + 'nx.jpg',
    boxName + 'py.jpg',
    boxName + 'ny.jpg',
    boxName + 'pz.jpg',
    boxName + 'nz.jpg'];

  var mapEnvLoaded = new THREE.CubeTextureLoader().load(urls);
  mapEnvLoaded.format = THREE.RGBFormat;
  mapEnvLoaded.mapping = THREE.CubeRefractionMapping;
  var mapCloud = loader.load('images/earth_clouds_2048.png');
  var geometryCloud = earth.geometry.clone();
  geometryCloud.scale(1.01, 1.01, 1.01);
  var materialCloud = new THREE.MeshPhongMaterial({
    color: propertyController.defaultColor,
    specular: '#000000',
    shininess: 0.0,
    map: mapCloud,
    envMap: mapEnvLoaded,
    transparent: true,
    blending: THREE.AdditiveBlending
  });
  cloud = new THREE.Mesh(geometryCloud, materialCloud);
  scene.add(cloud);

  // position initialization
  group = new THREE.Group();
  scene.add(group);
  group.rotation.z = 30;
  pointLight1.position.set(100, 40, 0);
  pointLight2.position.set(-100, -30, 0);
  group.add(pointLight1);
  group.add(pointLight2);
}

/**
 * Gets mouse position (x,y) when mouse move event occurs.
 * @param {MouseEvent} event MouseEvent object generated when mouse moves.
 */
function onMouseMove(event) {
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
}

/**
 * Create Earth Object based on the settings specified by the controller.
 */
function createNewEarth() {
  var currentRotation = 0;
  if (earth !== undefined) {
    currentRotation = earth.rotation.y;
    earth.geometry.dispose();
    earth.material.dispose();
    scene.remove(earth);
  }

  // material settings, using texture maps
  var mapColor;
  var mapNormal;
  var mapSpec;
  if (propertyController.colorMap) {
    mapColor = mapColorLoaded;
  } else {
    mapColor = null;
  }
  if (propertyController.specularMap) {
    mapSpec = mapSpecLoaded;
  } else {
    mapSpec = null;
  }
  if (propertyController.normalMap) {
    mapNormal = mapNormalLoaded;
  } else {
    mapNormal = null;
  }

  var materialEarth = new THREE.MeshPhongMaterial({
    color: propertyController.defaultColor,
    specular: '#333333',
    shininess: propertyController.shininess,
    map: mapColor,
    normalMap: mapNormal,
    specularMap: mapSpec,
    reflectivity: 0.6,
    refractionRatio: 0.6
  });

  // object3D setting
  var geometrySphere = new THREE.SphereGeometry(50, 100, 100);
  earth = new THREE.Mesh(geometrySphere, materialEarth);
  earth.rotation.y = currentRotation;
  scene.add(earth);
}

/**
 * Called after small updates on the property controller.
 * Here small means it doesn't require creation of new Mesh.
 */
function controlerUpdate() {
  cloud.material.reflectivity = propertyController.reflectivity;
  cloud.material.refractionRatio = propertyController.refractionRatio;
  cloud.material.opacity = propertyController.opacity;
  earth.material.shininess = propertyController.shininess;
  earth.material.emissive.set(propertyController.emissiveColor);
  sunLight.position.set(propertyController.lx * 1000, propertyController.ly * 1000, propertyController.lz * 1000);
  flare.position.copy(sunLight.position);
  ambient.color.set(propertyController.ambientColor);
  render();
  //  directionalLight.position.set(propertyController.lx, propertyController.ly, propertyController.lz);
}



/**
 * Rendering function, in which information of positions, ray caster, camera control are updated.
 */
function render() {
  earth.rotation.y += propertyController.rotateSpeed;
  cloud.rotation.y += propertyController.rotateSpeed * 1.4;
  group.rotation.y += 0.03;
  controls.update();

  raycaster.setFromCamera(mouse, camera);
  var intersects = raycaster.intersectObjects(scene.children);
  if (intersects.length > 0) {
    if (INTERSECTED !== intersects[0].object) {
      if (INTERSECTED) {
        INTERSECTED.material.color.set(propertyController.defaultColor);
      }
      INTERSECTED = intersects[0].object;
      INTERSECTED.material.color.set(propertyController.selectedColor);
    }
  } else {
    earth.material.color.set(propertyController.defaultColor);
    // if (INTERSECTED) {
    //   INTERSECTED.material.color.set(defaultColor);
    // }
    INTERSECTED = null;
  }
  renderer.render(scene, camera);
}

/**
 * Animate using render() function every frame, and make loop by call itself .
 */
function anime() {
  requestAnimationFrame(anime);
  render();
}
